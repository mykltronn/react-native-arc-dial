import React, { Component } from "react";
import styled from "styled-components";
import { View } from "react-native";
import PropTypes from "prop-types";

// children
import DialBar from "./DialBar";
// import DialCircle from "./DialCircle";

/**
|--------------------------------------------------
| Styles
|--------------------------------------------------
*/
const ContainerStyled = styled.View`
  justify-content: center;
  align-items: center;
`;

class Dial extends Component {
  state = {
    containerWidth: "",
    containerHeight: "",
    containerStartX: "",
    containerStartY: ""
  };

  render() {
    const {
      containerWidth,
      containerHeight,
      containerStartX,
      containerStartY
    } = this.state;

    const dialRadius = (containerWidth - 4 * this.props.barWidth) / 2;
    return (
      <ContainerStyled
        onLayout={event => {
          const { width, height } = event.nativeEvent.layout;
          this.setState({
            containerWidth: width,
            containerHeight: height
          });
        }}
      >
        {!!containerWidth && (
          <DialBar
            // dial bits
            width={containerWidth}
            barWidth={this.props.barWidth}
            barColor={this.props.barColor}
            // circle bits
            circleWidth={this.props.circleWidth}
            circleFillColor={this.props.circleFillColor}
            circleStrokeColor={this.props.circleStrokeColor}
            circleStrokeWidth={this.props.circleStrokeWidth}
            onChange={this.props.onChange}
            value={this.props.value || 0}
            // circle mathy bits
            dialRadius={dialRadius}
            dialOffsetY={this.props.barWidth}
            containerWidth={containerWidth}
            containerHeight={containerHeight}
            containerStartX={containerStartX}
            containerStartY={containerStartY}
            circleWidth={this.props.circleWidth}
            valueMin={this.props.valueMin}
            valueMax={this.props.valueMax}
            onRelease={this.props.onRelease}
          />
        )}
      </ContainerStyled>
    );
  }
}

Dial.defaultProps = {
  circleWidth: 40,
  circleFillColor: "rgba(0, 0, 0, 0.9)",
  circleStrokeColor: "black",
  circleStrokeWidth: 2,
  barWidth: 15,
  barColor: "grey",
  valueMin: 0,
  valueMax: 100,
  onRelease: () => console.log("Gesture released"),
  value: 0
};

Dial.propTypes = {
  circleWidth: PropTypes.number,
  circleFillColor: PropTypes.string,
  circleStrokeColor: PropTypes.string,
  circleStrokeWidth: PropTypes.number,
  barWidth: PropTypes.number,
  barColor: PropTypes.string,
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  valueMin: PropTypes.number.isRequired,
  valueMax: PropTypes.number.isRequired,
  onRelease: PropTypes.func
};

export default Dial;
