## React-Native Arc Dial

### Steps to demo
1. Clone repo
2. Get react-native-cli if you don't already have it:
    - `npm i -g react-native-cli`
2. Install node modules
    - `npm i`
3. Run the project on an emulator or device
    - `react-native run-android`
    - `react-native run-ios` (not yet tested)