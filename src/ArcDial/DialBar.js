import React, { Component } from "react";
import { PanResponder } from "react-native";
import PropTypes from "prop-types";
import styled from "styled-components";
// Deps
import Svg, { Circle, Path } from "react-native-svg";

const StyledView = styled.View``;

const CIRCLE_EXPAND_RATIO = 1.5;
const CIRCLE_TOUCH_RADIUS = 40;

class DialBar extends Component {
  state = {
    circleCenterX: "",
    circleCenterY: "",
    startAngle: "",
    circleWidth: this.props.circleWidth
  };

  // componentDidMount = () => {
  panResponder = PanResponder.create({
    // onMoveShouldSetPanResponder: () => true,
    // onMoveShouldSetPanResponderCapture: () => true,
    onStartShouldSetPanResponder: () => true,
    onStartShouldSetPanResponderCapture: () => true,
    onPanResponderGrant: () => {
      this.setCircleCenter();
      this.adjustCircleOnTouch(true);
    },
    onPanResponderMove: (event, _gesture) =>
      this.handlePanResponderMove(event, _gesture),
    onPanResponderRelease: () => {
      this.adjustCircleOnTouch(false);
      this.props.onRelease && this.props.onRelease();
    }
  });

  handlePanResponderMove = (event, _gesture) => {
    const { circleCenterX, circleCenterY } = this.state;
    // const { angleLength, startAngle, onUpdate } = this.props;
    const { locationX, locationY } = event.nativeEvent;
    const { onChange, valueMin, valueMax, dialRadius } = this.props;

    const xRelativeToCx = locationX - circleCenterX;
    const yRelativeToCy = locationY - circleCenterY;

    const distanceToCenter = Math.sqrt(
      xRelativeToCx * xRelativeToCx + yRelativeToCy * yRelativeToCy
    );

    const outerLimit = dialRadius + CIRCLE_TOUCH_RADIUS;
    const innerLimit = dialRadius - CIRCLE_TOUCH_RADIUS;

    if (distanceToCenter > outerLimit) {
      return;
    }

    if (distanceToCenter < innerLimit) {
      return;
    }

    if (distanceToCenter <= outerLimit && distanceToCenter >= innerLimit) {
      const radians = -Math.atan2(yRelativeToCy, xRelativeToCx);

      const value = this.radiansToValue(radians);

      if (value <= valueMin) return;
      if (value >= valueMax) return;
      else {
        const roundedValue = Math.round(value);
        onChange(roundedValue);
      }
    }
  };

  adjustCircleOnTouch = shouldEnlarge => {
    const { circleWidth } = this.props;
    if (shouldEnlarge) {
      this.setState({ circleWidth: circleWidth * CIRCLE_EXPAND_RATIO });
    } else {
      this.setState({ circleWidth });
    }
  };

  setCircleCenter = () => {
    const { containerWidth, dialOffsetY, containerHeight } = this.props;
    this.setState({
      circleCenterX: containerWidth / 2,
      circleCenterY: containerHeight - dialOffsetY
    });
  };

  valueToRadians = dirtyValue => {
    const { valueMin, valueMax } = this.props;
    const sanitizedValue = parseInt(dirtyValue, 10);
    const oldValue = -sanitizedValue;

    const oldMin = valueMin;
    const oldMax = valueMax;

    const newMin = Math.PI;
    const newMax = 2 * Math.PI;

    let radians;
    const oldRange = oldMax - oldMin;
    if (oldRange === 0) radians = newMin;
    else {
      const newRange = newMax - newMin;
      radians = ((oldValue - oldMin) * newRange) / oldRange + newMin;
    }
    return radians;
  };

  radiansToValue = radians => {
    const { valueMin, valueMax } = this.props;
    const oldValue = radians;

    const oldMin = Math.PI;
    const oldMax = 0;

    const newMin = valueMin;
    const newMax = valueMax;

    let value;
    const oldRange = oldMax - oldMin;
    if (oldRange === 0) value = newMin;
    else {
      const newRange = newMax - newMin;
      value = ((oldValue - oldMin) * newRange) / oldRange + newMin;
    }
    return value;
  };

  render() {
    const { width, barWidth, barColor } = this.props;
    /**
    |--------------------------------------------------
    | Bar mathy bits
    |--------------------------------------------------
    */

    // const height = width * 0.6;
    const height = width * 0.7;
    const startX = barWidth * 2;
    const endX = width - startX;

    const rx = 1;
    const ry = 1;

    const bottomOffset = height - barWidth * 2;

    const startY = bottomOffset;
    const endY = bottomOffset;

    const d = `M ${startX} ${startY} A ${rx} ${ry} 0 0 1 ${endX} ${endY}`;

    /**
    |--------------------------------------------------
    | Circle mathy bits
    |--------------------------------------------------
    */
    const {
      circleFillColor,
      circleStrokeColor,
      circleStrokeWidth,
      dialRadius,
      dialOffsetY,
      value,
      valueMin,
      valueMax
    } = this.props;

    const circleWidth = this.state.circleWidth;

    let panic;
    let sanitizedValue = parseInt(value, 10);
    if (isNaN(sanitizedValue)) panic = true;
    if (value < valueMin || value > valueMax) panic = true;

    const thetaValue = this.valueToRadians(value);
    const thetaX = dialRadius * Math.cos(thetaValue);
    const thetaY = -dialRadius * Math.sin(thetaValue);

    return (
      <StyledView>
        <Svg height={height} width={width}>
          {/* Bar */}
          <Circle cx={startX} cy={startY} r={barWidth / 2} fill={barColor} />
          <Path d={d} fill="none" stroke={barColor} strokeWidth={barWidth} />
          <Circle cx={endX} cy={endY} r={barWidth / 2} fill={barColor} />

          {/* Circle */}
          {!panic && (
            <Circle
              r={circleWidth / 2}
              fill={circleFillColor}
              stroke={circleStrokeColor}
              strokeWidth={circleStrokeWidth}
              cx={width / 2}
              cy={bottomOffset}
              // cy={height - dialOffsetY}
              transform={`translate(${thetaX}, ${thetaY})`}
              {...this.panResponder.panHandlers}
            />
          )}
        </Svg>
      </StyledView>
    );
  }
}

DialBar.propTypes = {
  width: PropTypes.number,
  barWidth: PropTypes.number,
  barColor: PropTypes.string,
  onRelease: PropTypes.func
};

export default DialBar;
