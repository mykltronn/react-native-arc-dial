
import React, { Component } from 'react';
import { Text, View } from 'react-native';

// Deps
import { Input, Button } from 'react-native-elements';

// Styles
import styled from "styled-components";

// Children
import ArcDial from './src/ArcDial/ArcDial';
import { interpolate } from './src/util/interpolate';


const ContainerStyled = styled.View`
  flex: 1;  
  justify-content: space-around;
  align-items: center;
  background-color: #F5FCFF;
`;

const ArcWrapperStyled = styled.View`
  width: 300px;
`;

const WelcomeText = styled.Text`
  font-size: 20;
  text-align: center;
  margin: 10px;
`;

const InstructionsText = styled.Text`
  text-align: center;
  color: #333333;
  margin-bottom: 5px;
`;

const TextWrapper = styled.View``;

const ValueText = styled.Text`
font-size: 26;
  text-align: center;
  color: #333333;
  /* margin-bottom: 10px; */
`;

const InputsWrapper = styled.View`
  width: 25%;
  height: 30%;
  justify-content: space-between;
`;




class App extends Component {
  state = {
    valueMin: "0",
    valueMax: "100",
    setValueMin: 0,
    setValueMax: 100,
    value: 0,
    error: ""
  };

  handleChangeText = (value, type) => {
    // if (valueisNaN(parseInt(value, 10))) return
    // else {
    this.setState({ error: "", [type]: value });
    // }
  }

  handleSetPressed = () => {
    this.setState({ error: "" })
    const { valueMin, valueMax, setValueMin, setValueMax, value } = this.state;
    const newValueMin = parseInt(valueMin, 10);
    const newValueMax = parseInt(valueMax, 10)

    if (!valueMin || !valueMax) return

    if (newValueMax <= newValueMin) {
      this.setState({ error: "Minimum Value must be less than Maxium Value..." })
    } else {
      const interpolatedValue = interpolate(
        [setValueMin, setValueMax],
        [newValueMin, newValueMax],
        value
      );

      this.setState({
        setValueMin: newValueMin,
        setValueMax: newValueMax,
        value: interpolatedValue
      })
    }
  }

  render() {
    return (
      <ContainerStyled >
        <TextWrapper>
          <WelcomeText >React-Native Arc Dial</WelcomeText>
          <InstructionsText>Built by Michael Ashton</InstructionsText>
        </TextWrapper>

        <InputsWrapper>
          <Input
            label="Minimum Value"
            keyboardType="number-pad"
            value={this.state.valueMin}
            onChangeText={value => this.handleChangeText(value, "valueMin")}
            errorMessage={this.state.error}
          />
          <Input
            label="Maxium Value"
            keyboardType="number-pad"
            value={this.state.valueMax}
            onChangeText={value => this.handleChangeText(value, "valueMax")}
          />

          <Button
            title="Set"
            onPress={this.handleSetPressed}
          />
        </InputsWrapper>

        <ArcWrapperStyled>
          <ValueText>
            {this.state.value}
          </ValueText>
          <ArcDial
            valueMin={this.state.setValueMin}
            valueMax={this.state.setValueMax}
            value={this.state.value}
            onChange={value => this.setState({ value })}

            circleFillColor="rgb(52, 138, 211)"
            circleStrokeColor=""
            barColor="#333333"
          />
        </ArcWrapperStyled>
      </ContainerStyled>
    );
  }
}

export default App;
