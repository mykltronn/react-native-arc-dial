/**
 * @param {Array} previousRange [oldMin, oldMax]
 * @param {Array} newRange [newMin, newMax]
 * @param {integer} value current value to interpolate
 * returns new value interpolated on new scale range
 */
export const interpolate = (previousRange, newRange, oldValue) => {
  let newValue;

  const oldMin = previousRange[0];
  const oldMax = previousRange[1];

  const newMin = newRange[0];
  const newMax = newRange[1];

  const oldRange = oldMax - oldMin;
  if (oldRange === 0) newValue = newMin;
  else {
    const newRange = newMax - newMin;
    newValue = ((oldValue - oldMin) * newRange) / oldRange + newMin;
  }
  return newValue;
};
