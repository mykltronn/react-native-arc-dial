import React, { Component } from "react";
import styled from "styled-components";
import ArcDial from "./ArcDial";

/**
|--------------------------------------------------
|  Styles
|--------------------------------------------------
*/
const ArcWrapperStyled = styled.View`
  width: 300px;
`;

const ValueTextWrapper = styled.Text`

`;

class ArcDialWidget extends Component {
  state = { value: this.props.minValue };
  render() {
    return (
      <ArcWrapperStyled>
        <ValueTextWrapper>
          {this.state.value}
        </ValueTextWrapper>
        <ArcDial
          {...this.props}
        />
      </ArcWrapperStyled>
    );
  }
}

export default ArcDialWidget;
